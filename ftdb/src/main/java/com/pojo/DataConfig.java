/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.pojo;


public class DataConfig {

    private String datacfg_id; //配置编号
    private Integer datacfg_brhnum; //网点数
    private Integer datacfg_sjnonum; //网点下科目数
    private Integer datacfg_accnum ; //科目下账号数
    private Integer datacfg_cusnum ; //客户数

    public String getDatacfg_id() {
        return datacfg_id;
    }

    public void setDatacfg_id(String datacfg_id) {
        this.datacfg_id = datacfg_id;
    }

    public Integer getDatacfg_brhnum() {
        return datacfg_brhnum;
    }

    public void setDatacfg_brhnum(Integer datacfg_brhnum) {
        this.datacfg_brhnum = datacfg_brhnum;
    }

    public Integer getDatacfg_sjnonum() {
        return datacfg_sjnonum;
    }

    public void setDatacfg_sjnonum(Integer datacfg_sjnonum) {
        this.datacfg_sjnonum = datacfg_sjnonum;
    }

    public Integer getDatacfg_accnum() {
        return datacfg_accnum;
    }

    public void setDatacfg_accnum(Integer datacfg_accnum) {
        this.datacfg_accnum = datacfg_accnum;
    }

    public Integer getDatacfg_cusnum() {
        return datacfg_cusnum;
    }

    public void setDatacfg_cusnum(Integer datacfg_cusnum) {
        this.datacfg_cusnum = datacfg_cusnum;
    }
}
